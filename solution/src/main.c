/**
 * @file
 * @brief Main application file
 */
#include <stdint.h>	
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

/**
 * @struct PEHeader
 * @brief PE file header structure
 */
struct 
#ifdef 	__GNUC__
	__attribute__((packed))
#endif 
PEHeader
{
	uint16_t machine;                /**< Machine type */
	uint16_t number_of_sections;     /**< Number of sections */
	uint32_t timestamp;              /**< Timestamp */
	uint32_t symbol_table_offset;    /**< Symbol table offset */
	uint32_t number_of_symbols;      /**< Number of symbols */
	uint16_t optional_header_size;   /**< Optional header size */
	uint16_t characteristics;        /**< Characteristics */
};

/**
 * @struct SectionHeader
 * @brief Section header structure
 */
struct 
#ifdef 	__GNUC__
	__attribute__((packed))
#endif 
SectionHeader
{
	char     name[8];                /**< Section name */
	uint32_t virtual_size;           /**< Virtual size */
	uint32_t virtual_address;        /**< Virtual address */
	uint32_t size_of_raw_data;       /**< Size of raw data */
	uint32_t pointer_to_raw_data;    /**< Pointer to raw data */
	uint32_t pointer_to_relocations; /**< Pointer to relocation entries */
	uint32_t pointer_to_linenumbers; /**< Pointer to line numbers */
	uint16_t number_of_relocations;  /**< Number of relocation entries */
	uint16_t number_of_linenumbers;  /**< Number of line numbers */
	uint32_t characteristics;        /**< Characteristics */
};


/**
 * @struct PEFile
 * @brief Structure containing PE file data
 */
struct 
#ifdef 	__GNUC__
	__attribute__((packed))
#endif 	
PEFile
{
	/// @name Offsets within file
	///@{

	/// Offset to a file magic
	uint32_t magic_offset; 
	/// Offset to a main PE header
	uint32_t header_offset; 
	/// Offset to a section table
	uint32_t section_header_offset; 
	///@}

	/// @name File headers
	///@{

	/// File magic
	char magic[5]; 
	/// Main header
	struct PEHeader header; 
	/// Array of section headers with the size of header.number_of_sections
	struct SectionHeader* section_headers; 
	///@}
};

/**
 * @brief Find a section in the PE file.
 *
 * This function searches for a section with the specified name in the given PE file.
 *
 * @param[in] PE_file Pointer to the PE file structure.
 * @param[in] section_name Name of the section to find.
 * @param[out] section_index Pointer to store the index of the found section.
 * @return 1 if the section is found, 0 otherwise.
 *
 * @note This function assumes that the section headers in the PE file have already been read and stored
 * in the `section_headers` field of the `PEFile` structure.
 */
int find_section(struct PEFile* PE_file, const char* section_name, uint32_t* section_index) {
	/**
	 * @brief Loop through the section headers to find the section.
	 *
	 * The function compares the name of each section header with the specified section name.
	 * If a match is found, it stores the index of the section in the `section_index` pointer
	 * and returns 1. If no match is found, it returns 0.
	 */
	for (uint16_t i = 0; i < PE_file->header.number_of_sections; ++i) 
		if (!(strcmp(PE_file->section_headers[i].name, section_name))) {
			*section_index = i;
			return 1; // Section is found
		}
	
	return 0; // Section is not found
}


#ifdef _MSC_VER
#pragma pack(pop)
#endif

/**
 * @brief Application entry point.
 *
 * This is the main function of the application. It performs the extraction of a specific section from a PE file
 * and writes it to an output binary file.
 *
 * @param[in] argc Number of command line arguments.
 * @param[in] argv Command line arguments.
 * @return 0 in case of success or error code.
 */
int main(int argc, char** argv) {
	/**
	 * @brief Checks if the correct number of arguments are provided.
	 *
	 * If not, prints the usage information to the console and exits with a code of 1.
	 */
	if (argc != 4) {
		return 1;
	}

	/**
	 * @brief Stores the path of the PE file provided as an argument.
	 */
	const char* source_pe_file = argv[1];
	/**
	 * @brief Stores the name of the section to extract from the PE file.
	 */
	const char* section_name = argv[2];
	/**
	 * @brief Stores the name of the output binary file where the section data will be written.
	 */
	const char* output_bin = argv[3];

	/**
	 * @brief Opens the PE file for reading in binary mode.
	 *
	 * If the file cannot be opened, prints an error message and exits with a code of 1.
	 */
	FILE* PE_f = fopen(source_pe_file, "rb");
	if (PE_f == NULL) {
		return 1;
	}

	/**
	 * @brief Declares a structure to store the data of the PE file.
	 */
	struct PEFile PE_file;

	/**
	 * @brief Reads the offset to the PE file's magic number.
	 */
	fseek(PE_f, 0x3c, SEEK_SET);
	fread(&PE_file.magic_offset, sizeof(PE_file.magic_offset), 1, PE_f);

	/**
	 * @brief Reads the PE file's magic number.
	 */
	fseek(PE_f, PE_file.magic_offset, SEEK_SET);
	fread(&PE_file.magic, sizeof(char), 4, PE_f);
	
	PE_file.magic[4] = '\0';

	/**
	 * @brief Verifies that the file is a PE file by checking its magic number.
	 *
	 * If it is not a PE file, closes the file, prints an error message, and exits with a code of 1.
	 */
	if (strcmp(PE_file.magic, "PE\0\0") != 0) {
		fclose(PE_f);
		return 1; 
	}

	/**
	 * @brief Calculates and stores the offset to the PE header.
	 */
	PE_file.header_offset = PE_file.magic_offset + 4;

	/**
	 * @brief Reads the PE header.
	 */
	fseek(PE_f, PE_file.header_offset, SEEK_SET);
	fread(&PE_file.header, sizeof(struct PEHeader), 1, PE_f);
	
	/**
	 * @brief Calculates and stores the offset to the section header.
	 */
	PE_file.section_header_offset = PE_file.header_offset + sizeof(struct PEHeader) + PE_file.header.optional_header_size;

	/**
	 * @brief Allocates memory for the section headers and reads them into memory.
	 *
	 * If memory cannot be allocated, closes the file, prints an error message, and exits with a code of 1.
	 */
	PE_file.section_headers = malloc(sizeof(struct SectionHeader) * PE_file.header.number_of_sections);
	if (PE_file.section_headers == NULL) {
		fclose(PE_f);
		return 1;
	}


	fseek(PE_f, PE_file.section_header_offset, SEEK_SET);
	fread(PE_file.section_headers, sizeof(struct SectionHeader), PE_file.header.number_of_sections, PE_f);

	/**
	 * @brief Finds the index of the specified section in the array of section headers.
	 *
	 * If the section is not found, frees the memory allocated for the section headers, closes the file,
	 * prints an error message, and exits with a code of 1.
	 */
	uint32_t section_index;
	if (!find_section(&PE_file, section_name, &section_index))
	{
		free(PE_file.section_headers);
		fclose(PE_f);
		return 1;
	}

	/**
	 * @brief Reads the data of the specified section into memory.
	 *
	 * If memory cannot be allocated, frees the memory allocated for the section headers, closes the file,
	 * prints an error message, and exits with a code of 1.
	 */
	fseek(PE_f, PE_file.section_headers[section_index].pointer_to_raw_data, SEEK_SET);

	uint8_t* section_data = malloc(PE_file.section_headers[section_index].size_of_raw_data);
	if (section_data == NULL)
	{
		free(PE_file.section_headers);
		fclose(PE_f);
		return 1;
	}

	fread(section_data, PE_file.section_headers[section_index].size_of_raw_data, 1, PE_f);

	/**
	 * @brief Closes the PE file after reading.
	 */
	fclose(PE_f);

	/**
	 * @brief Opens the output file for writing in binary mode.
	 *
	 * If the file cannot be opened, frees the memory allocated for the section headers and the section data,
	 * prints an error message, and exits with a code of 1.
	 */
	FILE* output_file = fopen(output_bin, "wb");
	if (output_file == NULL)
	{
		free(PE_file.section_headers);
		free(section_data);
		return 1;
	}

	/**
	 * @brief Writes the section data to the output file.
	 */
	fwrite(section_data, PE_file.section_headers[section_index].size_of_raw_data, 1, output_file);

	/**
	 * @brief Closes the output file after writing.
	 */
	fclose(output_file);

	/**
	 * @brief Frees the memory allocated for the section headers and the section data.
	 */
	free(PE_file.section_headers);
	free(section_data);

	/**
	 * @brief Returns 0 to indicate that the program executed successfully.
	 */
	return 0;
}
